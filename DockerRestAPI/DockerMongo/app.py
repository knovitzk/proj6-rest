import os
import sys
import flask 
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow 
import acp_times # brevet time calculations
import logging
import random


app = Flask(__name__)
app.secret_key = b'#\x1bA\xcd[\xa1\xb9'

client = MongoClient('db', 27017)
db = client.tododb
collection = db.control

collection.delete_many({})

@app.route("/")
def root():
    collection.delete_many({})
    return flask.render_template('calc.html')


@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/_calc_times")
def _calc_times():
    app.logger.debug("Got a JSON request")

    km = request.args.get('km', 999, type=float)
    brevet_dist = request.args.get('brevet_dist', 0, type=float)
    begin_date = request.args.get('begin_date', 0, type=str)
    begin_time = request.args.get('begin_time', 0, type=str)
    
    message = ""
    
    app.logger.debug("km = {}".format(km))
    app.logger.debug("brevet_dist = {}".format(brevet_dist))
    app.logger.debug("begin_date = {}".format(begin_date))
    app.logger.debug("begin_time = {}".format(begin_time))

    if km < 0:
      message = "Error- Control distance cannot be negative."
      km = 0

    if km > (brevet_dist * 1.2):
      message = "Error- Control distance cannot be longer than 120% brevet distance."


    brevet_start = begin_date + " " + begin_time + ":00"
    brevet_start_time = arrow.get(brevet_start, 'YYYY-MM-DD HH:mm:ss')

    open_time = acp_times.open_time(km, brevet_dist, brevet_start_time.isoformat())
    close_time = acp_times.close_time(km, brevet_dist, brevet_start_time.isoformat())
    result = {"open": open_time, "close": close_time, "message": message}

    return flask.jsonify(result=result)

@app.route('/display')
def display():
    app.logger.debug("Displaying times.")
    
    flask.g.kms = []
    flask.g.open = []
    flask.g.close = []
    
    for entry in collection.find():
        flask.g.kms.append(entry['km'])
        flask.g.open.append(entry['open_time'])
        flask.g.close.append(entry['close_time'])

    return flask.render_template("display.html")

@app.route('/new', methods = ['POST'])
def new():
    
    open_time_list = request.form.getlist("open")
    close_time_list = request.form.getlist("close")
    km_list = request.form.getlist("km")

    if km_list == []:
        flask.flash("List is empty!")
        #Redirect back to '/index'
        return flask.redirect(flask.url_for("index"))

    for item in range(20):
        if km_list[item] == '':
            continue
        
        record = {
        'open_time' : open_time_list[item],
        'close_time' : close_time_list[item],
        'km' : km_list[item]
        }
        
        #Insert to database
        collection.insert_one(record)

    flask.flash("Data is saved")
    flask.flash("Click on 'Display' to view results")
    
    
    #Redirect back to '/index'
    return flask.redirect(flask.url_for("index"))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=5000)
